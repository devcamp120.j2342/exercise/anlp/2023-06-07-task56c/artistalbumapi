package com.devcamp.artistalbumapi.services;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.artistalbumapi.models.Artist;


@Service
public class ArtistService {
    @Autowired
    AlbumService albumService;

    Artist casi1 = new Artist(1, "Justin Bieber");
    Artist casi2 = new Artist(2, "The weeknd");
    Artist casi3 = new Artist(3, "One Direction");

    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> allArtist = new ArrayList<>();

        casi1.setAlbums(albumService.getAlbum1());
        casi2.setAlbums(albumService.getAlbum2());
        casi3.setAlbums(albumService.getAlbum3());

        allArtist.add(casi1);
        allArtist.add(casi2);
        allArtist.add(casi3);

        return allArtist;
    }

    public Artist getArtistInfo(@RequestParam(name="code", required = true) int id){
        ArrayList<Artist> allArrtist = getAllArtist();

        Artist findArtist = new Artist();

        for (Artist artistElement : allArrtist) {
            if(artistElement.getId() == id){
                findArtist = artistElement;
                break;
            }
        }
        return findArtist;
    }

}
