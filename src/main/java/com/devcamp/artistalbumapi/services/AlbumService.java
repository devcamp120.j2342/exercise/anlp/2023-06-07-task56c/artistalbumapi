package com.devcamp.artistalbumapi.services;
import java.util.ArrayList;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.artistalbumapi.models.Album;

@Service
public class AlbumService {
    
    public ArrayList<String> createSongs1() {
        ArrayList<String> songs1 = new ArrayList<>();
        songs1.add("bai hat 1");
        songs1.add("bai hat 2");
        songs1.add("bai hat 3");
        return songs1;
    }

    public ArrayList<String> createSongs2() {
        ArrayList<String> songs2 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs2
        songs2.add("ten bai hat 1");
        songs2.add("ten bai hat 2");
        songs2.add("ten bai hat 3");
        return songs2;
    }

    public ArrayList<String> createSongs3() {
        ArrayList<String> songs3 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs3.add("ten bai hat 1");
        songs3.add("ten bai hat 2");
        songs3.add("ten bai hat 3");
        return songs3;
    }

    Album myWorld = new Album(1, "My World 2.0", createSongs1());
    Album purpose = new Album(2, "Purpose", createSongs1());
    Album Changes = new Album(3, "Changes", createSongs1());

    Album StartBoy = new Album(4, "StartBoy", createSongs2());
    Album dawnFM = new Album(5, "Dawn FM", createSongs2());
    Album afterHours = new Album(6, "After Hours", createSongs2());

    Album four = new Album(7, "Four", createSongs3());
    Album takeMeHome = new Album(8, "TTake me home", createSongs3());
    Album upAllNight = new Album(9, "Up All Night", createSongs3());

    public ArrayList<Album> getAlbum1(){
        ArrayList<Album> album1 = new ArrayList<>();

        album1.add(myWorld);
        album1.add(purpose);
        album1.add(Changes);

        return album1;
    }

    public ArrayList<Album> getAlbum2(){
        ArrayList<Album> album2 = new ArrayList<>();

        album2.add(StartBoy);
        album2.add(dawnFM);
        album2.add(afterHours);

        return album2;
    }

    public ArrayList<Album> getAlbum3(){
        ArrayList<Album> album3 = new ArrayList<>();

        album3.add(four);
        album3.add(takeMeHome);
        album3.add(upAllNight);

        return album3;
    }

    public ArrayList<Album> getAllAlbum(){
        ArrayList<Album> allAlbum = new ArrayList<>();

        allAlbum.add(myWorld);
        allAlbum.add(purpose);
        allAlbum.add(Changes);
        allAlbum.add(StartBoy);
        allAlbum.add(dawnFM);
        allAlbum.add(afterHours);
        allAlbum.add(four);
        allAlbum.add(takeMeHome);
        allAlbum.add(upAllNight);
        

        return allAlbum;
    }

    public Album filterAlbum(@RequestParam(name="code", required = true) int id){
        ArrayList<Album> album = getAllAlbum();

        Album findAlbum = new Album();

        for (Album albumElement : album) {
            findAlbum = albumElement;
            break;
        }
        
        return findAlbum;
    }

}
